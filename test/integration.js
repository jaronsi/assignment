const chai = require('chai');
const chaiHttp = require('chai-http');
chai.use(chaiHttp);

const app = require('../index.js');
const should = chai.should();

let login_details = {
  'username':'john',
  'email':'john@mail.com', 
  'password':'john'
};

//Publically available
//Home
describe('GET /', () => {
    it('should successfully access the homepage.', done => {
      chai.request(app).get('/').end((err,res)=>{
      res.should.have.status(200);
      done();
    });
  });
});

//New Post
describe('GET /posts/new', () => {
    it('should fail to load the new post page.', done => {
      chai.request(app).get('/posts/new').end((err,res)=>{
      res.should.have.status(200);
      done();
    });
  });
});

//Login
describe('GET /', () => {
    it('should successfully access the login page.', done => {
      chai.request(app).get('/auth/login').end((err,res)=>{
      res.should.have.status(200);
      done();
    });
  });
});

//Register
describe('GET /', () => {
      it('should successfully access the register page.', done => {
      chai.request(app).get('/auth/register').end((err,res)=>{
      res.should.have.status(200);
      done();
    });
  });
});

//-----------------------------------------------------------------//
//Require authentication
describe('POST /users/register', () => {
  it('should successfully register.', done => {
    chai.request(app).post('/users/register').send(login_details).end((err,res) => {
      res.should.have.status(200);
      done();
    });
  });
});


describe('POST /users/login', () => {
    it('should successfully login.', done => {
      chai.request(app).post('/users/login').send(login_details).end((err,res) => {
      res.should.have.status(200);
      done();
    });
  });
});

describe('POST /posts/new', () => {
    it('should successfully access the new post page.', done => {
      var agent = chai.request.agent(app);
      agent.post('/users/login').send(login_details).then(function(res){
        agent.get('/posts/new').then(function(res2){
        res2.should.have.status(200);
        done();
      });
    });
  });
});
