//Dependencies
const path = require('path');
const {engine} = require('express-edge');
const express = require('express');
const edge = require('edge.js');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const expressSession = require('express-session');
const connectMongo = require('connect-mongo');

//Malicious dependency - Patch by removing
const wordFormatter = require('./src/word-formatter');

//Controllers
const getPostController = require('./src/controller/getPost');
const homeController = require('./src/controller/home');
const loginController = require('./src/controller/login');
const loginUserController = require('./src/controller/loginUser');
const logoutUserController = require('./src/controller/logoutUser');
const newPostController = require('./src/controller/newPost');
const newUserController = require('./src/controller/newUser');
const createPostController = require('./src/controller/createPost');
const createUserController = require('./src/controller/createUser');



const app = express();
const port = process.env.PORT || 8000;

//Use below for local database
//mongoose.connect('mongodb://localhost:27017/isec-blog', {
//Use below for docker database
mongoose.connect('mongodb://mongo:27017/isec-blog', {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false,
  useCreateIndex: true
});

mongoose.connection.on('error', err => {
  console.log('err', err);
});

mongoose.connection.on('connected', () => {
  console.log('mongoose is connected');
});

//Storing sessions in database - unnecessary
const mongoStore = connectMongo(expressSession);
app.use(expressSession({
  secret: 'secret',
  store: new mongoStore({
    mongooseConnection: mongoose.connection
    })
  })
);

//Serving static files
app.use(express.static('public'));
app.use(engine);
app.set('views',path.join(__dirname,'src/views'));

app.use('*', (req, res, next) => {
  edge.global('auth', req.session.userId);
  next();
});

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended:true
}));

//Middleware
const storePost = require('./src/middleware/createPost');
const auth = require('./src/middleware/auth');
const redirectIfAuthenticated = require('./src/middleware/redirectIfAuthenticated');
app.use('posts/store',storePost);

//Http
app.get('/',homeController);
app.get('/post/:id',getPostController);
app.get('/posts/new',auth,newPostController);
app.post('/posts/store',auth,storePost,createPostController);
app.get('/auth/login',redirectIfAuthenticated,loginController);
app.post('/users/login',redirectIfAuthenticated,loginUserController);
app.get('/auth/register',redirectIfAuthenticated,newUserController);
app.post('/users/register',redirectIfAuthenticated,createUserController);
app.get('/auth/logout',logoutUserController);

app.listen(port,() => {
  console.log(`App listening on port ${port}`);
});

module.exports = app;