module.exports = (req, res) => {
  console.log('logged out');
  req.session.destroy(() => {
    console.log('logged out');
    res.redirect('/');
  });
};