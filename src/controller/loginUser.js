const bcrypt = require('bcrypt');
const User = require('../model/User');
module.exports = (req, res) => {
  const {
    email,
    password
  } = req.body;
  User.findOne({
    email
    }, (error, user) => {
    if (user) {
      bcrypt.compare(password, user.password, (error, same) => {
        if (error){
          console.log('ERROR:');
          console.log(error);
        }
        if (same) {
          req.session.userId = user._id;
          res.redirect('/');
        } 
        else {
            console.log('password error');
            res.redirect('/auth/login');
        }
      });
    }
    else {
      console.log('User not found');
      return res.redirect('/auth/login');
    }
  });
};