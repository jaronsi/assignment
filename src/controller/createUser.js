const path = require('path');
const User = require('../model/User');
 
module.exports = (req, res) => {
  User.create(req.body, (error, user) => {
  if (error) {
    console.log('Error');
    return res.redirect('/auth/register')
  }
  else{
    console.log('No error');
  }
  res.redirect('/');
  });
};