# A sample Makefile to use make command to build, test and run the program
# Guide: https://philpep.org/blog/a-makefile-for-your-dockerfiles/
# NOTE: the makefile creates a standard image for mongodb
# I do not automatically clean this up as it is a popular image you 
# may be using.  If you are not using the docker mongo image then uncomment the
# extra line.

APP=assignment_app
DB=mongo

test:
	docker-compose -f docker-compose.test.yml up --build --abort-on-container-exit
run:
	docker-compose up --build
clean:
	docker system prune
	docker image rm $(APP)
	docker image rm $(DB)

.PHONY: all test clean
